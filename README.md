## Dataset for torch-rnn on music generation ##
Link to download the full dataset: https://glcdn.githack.com/scheng123/dataset-for-char-rnn-music-generation/raw/master/all.ai

Processed files (TWO PARTS):<br>
H5: https://glcdn.githack.com/scheng123/dataset-for-char-rnn-music-generation/raw/master/all.h5<br>
JSON: https://glcdn.githack.com/scheng123/dataset-for-char-rnn-music-generation/raw/master/all.json